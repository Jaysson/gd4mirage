﻿using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public BoxCollider doorTrigger;
    public GameObject door;

    private float speed = 1.0f;
    private Vector3 originalPosition;
    private Vector3 destination;

    private bool moveToDest = false;
    private bool moveToOri = false;

    void Awake()
    {
        originalPosition = door.transform.localPosition;
        destination = originalPosition + new Vector3(0, 0, 1.3f);
        Debug.Log(originalPosition);
        Debug.Log(destination);
    }

    private void OnTriggerEnter(Collider doorTrigger)
    {
        moveToDest = true;
        moveToOri = false;
        Debug.Log("Entered");
    }

    private void OnTriggerExit(Collider doorCollider)
    {
        moveToDest = false;
        moveToOri = true;
        Debug.Log("Exitted");
    }

    void Update()
    {
        if(moveToDest && door.transform.position != destination)
        {
            door.transform.localPosition = Vector3.MoveTowards(door.transform.localPosition, destination, speed * Time.deltaTime);
        }
        if(moveToOri && door.transform.position != originalPosition)
        {
            door.transform.localPosition = Vector3.MoveTowards(door.transform.localPosition, originalPosition, speed * Time.deltaTime);
        }
        
        if(door.transform.position == destination)
        {
            moveToDest = false;
        }
        if(door.transform.position == originalPosition)
        {
            moveToOri = false;
        }
    }
}
