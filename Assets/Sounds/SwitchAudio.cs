﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchAudio : MonoBehaviour {

    public AudioClip newTrack;
    public AudioClip newTrack1;
    public AudioClip newTrack2;
    public AudioClip newTrack3;
    public AudioClip newTrack4;
    public int count = 1;

    private AudioControler theAC;

	// Use this for initialization
	void Start () {
        theAC = FindObjectOfType<AudioControler>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.N))
        {
            if(count == 1)
            {
                if (newTrack != null)
                {
                    theAC.ChangeBGM(newTrack);
                }
                count++;
            }
            else if(count == 2)
            {
                if (newTrack1 != null)
                {
                    theAC.ChangeBGM(newTrack1);
                }
                count++;
            }
            else if(count == 3)
            {
                if(newTrack2 != null)
                {
                    theAC.ChangeBGM(newTrack2);
                }
                count++;
            }
            else if (count == 4)
            {
                if (newTrack4 != null)
                {
                    theAC.ChangeBGM(newTrack3);
                }
                count++;
            }
            else if (count == 5)
            {
                if (newTrack4 != null)
                {
                    theAC.ChangeBGM(newTrack4);
                }
                count = 1;
            }
        }

    }
}
