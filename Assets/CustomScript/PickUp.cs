﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public Tutorials tutorials;
    public Transform onHand;
    public customaztion customaztion;
    public ModifiableModel modifableModel;
    public ModifiableManager modifableManager;
    public bool isHolding = false;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && (isHolding == true))
        {
            transform.Rotate(5, 0, 0);
            tutorials.hasRotated = 1;
        }
        if (Input.GetKeyDown(KeyCode.O) && (isHolding == true))
        {
            transform.Rotate(0, 5, 0);
            tutorials.hasRotated = 1;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            scaling();
            tutorials.hasScaled = 1;
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            descaling();
            tutorials.hasScaled = 1;
        }
    }
    void OnMouseDown()
    {
        GetComponent<Rigidbody>().useGravity = false;
        this.transform.position = onHand.position;
        this.transform.parent = GameObject.Find("Player").transform;
        this.transform.parent = GameObject.Find("MainCamera").transform;
        GetComponent<Rigidbody>().constraints =
                       RigidbodyConstraints.FreezeAll;
        isHolding = true;
        tutorials.hasPickedUp = 1;
        modifableManager.deleteList();
        modifableManager.models.Add(gameObject);
    } // end OnMouseDown

    void OnMouseUp()
    {
        this.transform.parent = null;
        this.GetComponent<Rigidbody>().constraints =
                       RigidbodyConstraints.None;
        this.GetComponent<Rigidbody>().useGravity = true;
        isHolding = false;

    }

    void scaling()
    {
        if (isHolding == true)
        {
            this.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        }

    }
    void descaling()
    {
        if (isHolding == true && (this.transform.localScale != new Vector3(0.1f, 0.1f, 0.1f)))
        {
            this.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        }
        else if (this.transform.localScale == new Vector3(0.1f, 0.1f, 0.1f))
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }

    }

}