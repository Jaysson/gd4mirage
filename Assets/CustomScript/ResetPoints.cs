﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPoints : MonoBehaviour { // this script will reset the player's position if they manage to escape the playable area.

    public GameObject player;
    public Vector3 resetPoint = new Vector3(0, 2.4f, 1.8f);
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (player.transform.position.y <= -1.5 || (player.transform.position.x >= 10.5) && (player.transform.position.z < -1))
        {
            player.transform.position = resetPoint;
        }
	}
}
