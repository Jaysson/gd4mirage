﻿using System;
using UnityEngine;

namespace RockVR.Rift
{
    public enum ControllerState
    {
        Ray,
        Touch,
        Normal
    }

    public enum SelectedOption
    {
        H,
        S,
        V
    }

    public class RIFT_Interaction : MonoBehaviour
    {
        [Tooltip("Laser Thickness")]
        public float laserThickness = 0.002f;
        [Tooltip("Laser HitScale")]
        public float laserHitScale = 0.02f;
        [Tooltip("Max Hit Distance")]
        public float maxDistance = 100.0f;
        public Color color;
        /// The selected object
        /// </summary>
        [NonSerialized]
        public GameObject selectedObject;
        /// <summary>
        /// Set the ray show or not show
        /// </summary>
        public bool show = true;
        /// <summary>
        /// Ray and object encounter point
        /// </summary>
        private GameObject hitPoint;
        /// <summary>
        /// The ray hit point
        /// </summary>
        private GameObject pointer;
        /// <summary>
        /// The Rigidbody of object
        /// </summary>
        private GameObject controllerRigidbodyObject;
        /// <summary>
        /// The limit distance
        /// </summary>
        private float distanceLimit;
        private OVRInput.Controller riftController;
        private SelectedOption selectedOption = SelectedOption.H;
        private GameObject pickedObject;

        private void Awake()
        {
            riftController = this.GetComponent<RIFT_EventCtrl>().riftController;
        }

        void Start()
        {
            CreateRay();
            //CreateControllerRigidBody();
        }

        void Update()
        {
            SetBoxColliderActive();

            if (OVRInput.GetDown(OVRInput.Button.Start) && !pointer.activeSelf)
            {
                pointer.SetActive(true);
            }
            else if (OVRInput.GetDown(OVRInput.Button.Start) && pointer.activeSelf)
            {
                pointer.SetActive(false);
            }

            if (show)
            {
                RayInteraction();
            }

            if (pickedObject != null)
                pickedObject.transform.localPosition = hitPoint.transform.position;
        }
        /// <summary>
        /// Creat ray and hit point
        /// </summary>
        void CreateRay()
        {
            pointer = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            pointer.transform.SetParent(transform, false);
            pointer.transform.localScale = new Vector3(laserThickness, laserThickness, 100.0f);
            pointer.transform.localPosition = new Vector3(0.0f, 0.0f, 50.0f);
            pointer.SetActive(false);
            hitPoint = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            hitPoint.transform.SetParent(transform, false);
            hitPoint.transform.localScale = new Vector3(laserHitScale, laserHitScale, laserHitScale);
            hitPoint.transform.localPosition = new Vector3(0.0f, 0.0f, 100.0f);
            hitPoint.SetActive(true);
            DestroyImmediate(hitPoint.GetComponent<SphereCollider>());
            DestroyImmediate(pointer.GetComponent<CapsuleCollider>());
            Material newMaterial = new Material(Shader.Find("RockVR/LaserPointer"));
            newMaterial.SetColor("_Color", color);
            pointer.GetComponent<MeshRenderer>().material = newMaterial;
            hitPoint.GetComponent<MeshRenderer>().material = newMaterial;
        }
        /// <summary>
        /// The ray interaction function
        /// </summary>
        void RayInteraction()
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hitInfo;
            bool bHit = Physics.Raycast(ray, out hitInfo, maxDistance);

            float distance = maxDistance;
            if (bHit && pickedObject == null)
            {
                distance = hitInfo.distance;
                selectedObject = hitInfo.collider.gameObject;
            }
            else
            {
                selectedObject = null;
            }

            if (pointer.activeSelf)
            {
                if (pickedObject != null)
                {
                    Vector2 axis = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
                    if (axis.y < -0.2)
                    {
                        if (maxDistance > 0.1f)
                        {
                            maxDistance -= 0.05f;
                        }
                    }
                    if (axis.y > 0.2)
                    {
                        if (maxDistance < 100.0f)
                        {
                            maxDistance += 0.05f;
                        }
                    }

                    if (OVRInput.GetDown(OVRInput.Button.One))
                    {
                        pickedObject.GetComponent<BoxCollider>().enabled = true;
                        if (pickedObject.tag != "stationary")
                            pickedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                        pickedObject = null;
                        maxDistance = 100.0f;
                    }
                    if (OVRInput.GetDown(OVRInput.Button.Two))
                    {
                        pickedObject.GetComponent<BoxCollider>().enabled = true;
                        if (pickedObject.tag != "stationary")
                            pickedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                        pickedObject.transform.rotation = Quaternion.Euler(0, pickedObject.transform.eulerAngles.y, 0);
                        pickedObject = null;
                        maxDistance = 100.0f;
                    }
                }
                else if (selectedObject != null && selectedObject.name == "Picker 2.0")
                {
                    Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
                    if (primaryAxis.y < -0.2)
                    {
                        switch (selectedOption)
                        {
                            case SelectedOption.H:
                                selectedObject.GetComponent<ColorPicker>().MinusH();
                                break;
                            case SelectedOption.S:
                                selectedObject.GetComponent<ColorPicker>().MinusS();
                                break;
                            case SelectedOption.V:
                                selectedObject.GetComponent<ColorPicker>().MinusV();
                                break;
                        }
                    }
                    if (primaryAxis.y > 0.2)
                    {
                        switch (selectedOption)
                        {
                            case SelectedOption.H:
                                selectedObject.GetComponent<ColorPicker>().PlusH();
                                break;
                            case SelectedOption.S:
                                selectedObject.GetComponent<ColorPicker>().PlusS();
                                break;
                            case SelectedOption.V:
                                selectedObject.GetComponent<ColorPicker>().PlusV();
                                break;
                        }
                    }
                    if (OVRInput.GetDown(OVRInput.Button.Three))
                    {
                        if (selectedOption != SelectedOption.V)
                        {
                            selectedOption++;
                            GameObject.Find("HSVSelected").GetComponent<UnityEngine.UI.Text>().text = "Selected: " + selectedOption.ToString();
                        }
                    }
                    if (OVRInput.GetDown(OVRInput.Button.Four))
                    {
                        if (selectedOption != SelectedOption.H)
                        {
                            selectedOption--;
                            GameObject.Find("HSVSelected").GetComponent<UnityEngine.UI.Text>().text = "Selected: " + selectedOption.ToString();
                        }
                    }
                }
                else if (selectedObject != null)
                {
                    if (OVRInput.GetDown(OVRInput.Button.One))
                    {
                        if (pickedObject == null && (selectedObject.tag == "interactable" || selectedObject.tag == "stationary"))
                        {
                            pickedObject = selectedObject;
                            pickedObject.GetComponent<BoxCollider>().enabled = false;
                            if (pickedObject.GetComponent<Rigidbody>() == null)
                                pickedObject.AddComponent<Rigidbody>();
                            pickedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                            maxDistance = 1.0f;
                            //maxDistance = Vector3.Distance(pointer.transform.localPosition, pickedObject.transform.localPosition);
                        }
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Two))
                    {
                        Color c = GameObject.Find("ColourPickerCube").GetComponent<Renderer>().material.color;

                        if (selectedObject.GetComponent<Renderer>())
                            selectedObject.GetComponent<Renderer>().material.color = c;
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Three))
                    {
                        selectedObject.transform.localScale -= new Vector3(0.01f, 0.01f, 0.01f);
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Four))
                    {
                        selectedObject.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                    }
                }
            }

            if (distanceLimit > 0.0f)
            {
                distance = Mathf.Min(distance, distanceLimit);
                bHit = true;
            }

            pointer.transform.localScale = new Vector3(laserThickness, laserThickness, distance);
            pointer.transform.localPosition = new Vector3(0.03f, 0.0f, distance * 0.5f);

            /*
            if (bHit)
            {
                hitPoint.SetActive(true);
                hitPoint.transform.localPosition = new Vector3(0.03f, 0.0f, distance);
            }
            else
            {
                hitPoint.SetActive(false);
            }
            */

            hitPoint.transform.localPosition = new Vector3(0.03f, 0.0f, distance);

            distanceLimit = -1.0f;
        }

        void SetBoxColliderActive()
        {
            foreach (var item in this.gameObject.GetComponents<BoxCollider>())
            {
                item.enabled = false;
            }
        }
    }
}