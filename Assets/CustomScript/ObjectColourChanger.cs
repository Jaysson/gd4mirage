﻿using UnityEngine;

public class ObjectColourChanger : MonoBehaviour
{
    public ColorPicker picker;

    // Use this for initialization
    void Start()
    {
        if (picker != null)
        {
            picker.onValueChanged.AddListener(color =>
            {
                gameObject.GetComponent<Renderer>().material.color = color;
            });
            gameObject.GetComponent<Renderer>().material.color = picker.CurrentColor;
        }

        //source: https://github.com/judah4/HSV-Color-Picker-Unity
    }

    void Update()
    {

    }
}