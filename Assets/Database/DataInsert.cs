﻿using UnityEngine;
using System.Collections;

public class DataInserter : MonoBehaviour
{

    public string inputUserName;
    public string inputComments;

    string CreateUserURL = "http://localhost/Jungian/Insert.php";

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) CreateUser(inputUserName, inputComments);
    }

    public void CreateUser(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);

        WWW www = new WWW(CreateUserURL, form);
    }
}
