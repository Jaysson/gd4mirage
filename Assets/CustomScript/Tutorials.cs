﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorials : MonoBehaviour
{

    public GameObject tutorial;
    public GameObject[] tutorialDialogue;
    public int currentDialogue = 0;
    public int checkIsMoving = 0;
    public int hasPickedUp = 0;
    public int hasRotated = 0;
    public int hasScaled = 0;
    public int hasAccMenu = 0;
    public int hasAddedSand = 0;
    public int hasDeletedSand = 0;
    public int hasAddedModels = 0;

    // Use this for initialization
    void Start()
    {
        tutorial.SetActive(true);
        for (int i = 0; i <= tutorialDialogue.Length - 1; i++)
        {
            tutorialDialogue[i].SetActive(false);
        }
        StartCoroutine(tutorialProgession());
    }

    // Update is called once per frame
    void Update()
    {
        if (currentDialogue == 1)
        {
            //Debug.Log("checking movement");
            checkMovement();
        }
    }

    IEnumerator tutorialProgession()
    {
        Debug.Log("in the method");
        setDialogueActive(); //intro dialogue
        yield return new WaitForSeconds(5);
        setDialogueInactive(); //intro dialogue
        setDialogueActive(); // movement dialogue
        yield return new WaitUntil(verifyHasMoved);
        if (checkIsMoving == 1)
        {
            setDialogueInactive(); //movement dialogue
        }
        setDialogueActive(); //pick-up dialogue
        yield return new WaitUntil(verifyHasPickedUp);
        if (hasPickedUp == 1)
        {
            setDialogueInactive(); //pick-up dialogue
        }
        setDialogueActive();//rotate dialogue
        yield return new WaitUntil(verifyHasRotated);
        if (hasRotated == 1)
        {
            setDialogueInactive();//rotate dialogue
        }
        setDialogueActive(); // scaling dialogue
        yield return new WaitUntil(verifyHasScaled);
        if (hasScaled == 1)
        {
            setDialogueInactive();//scaling dialogue
        }
        setDialogueActive();// access menu dialogue
        yield return new WaitUntil(verifyHasAddedSand);
        if (hasAddedSand == 1)
        {
            setDialogueInactive();//added sand dialogue
        }
        setDialogueActive();// deleted sand dialogue
        yield return new WaitUntil(verifyHasDeletedSand);
        if (hasDeletedSand == 1)
        {
            setDialogueInactive();//deleted sand dialogue
        }
        setDialogueActive();// access menu dialogue
        yield return new WaitUntil(verifyHasAccMenu);
        if (hasAccMenu == 1)
        {
            setDialogueInactive();//access menu dialogue
        }
        setDialogueActive();//adding models dialogue
        yield return new WaitUntil(verifyHasAddedModels);
        if (hasAddedModels == 1)
        {
            setDialogueInactive();//adding models dialogue
        }
        setDialogueActive();// ending tutorial dialogue
        yield return new WaitForSeconds(3);
        setDialogueInactive(); // ending tutorial dialogue
        tutorial.SetActive(false);
    }

    public void setDialogueActive()
    {
        tutorialDialogue[currentDialogue].SetActive(true);
    }

    public void setDialogueInactive()
    {
        tutorialDialogue[currentDialogue].SetActive(false);
        if (currentDialogue < 10)
        {
            currentDialogue++;
        }
        Debug.Log(currentDialogue);
    }

    public void checkMovement()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
        {
            checkIsMoving = 1;
        }
    }

    public bool verifyHasMoved()
    {
        if (checkIsMoving == 1)
        {
            return true;
        }
        return false;
    }

    public bool verifyHasPickedUp()
    {
        if (hasPickedUp == 1)
        {
            return true;
        }

        return false;
    }

    public bool verifyHasRotated()
    {
        if (hasRotated == 1)
        {
            return true;
        }

        return false;
    }
    public bool verifyHasScaled()
    {
        if (hasScaled == 1)
        {
            return true;
        }

        return false;
    }
    public bool verifyHasAccMenu()
    {
        if (hasAccMenu == 1)
        {
            return true;
        }

        return false;
    }
    public bool verifyHasAddedSand()
    {
        if (hasAddedSand == 1)
        {
            return true;
        }

        return false;
    }
    public bool verifyHasDeletedSand()
    {
        if (hasDeletedSand == 1)
        {
            return true;
        }

        return false;
    }
    public bool verifyHasAddedModels()
    {
        if (hasAddedModels == 1)
        {
            return true;
        }

        return false;
    }
}
