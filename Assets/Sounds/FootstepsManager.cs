﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsManager : MonoBehaviour {

    public AudioSource Footsteps;
    private Vector3 position;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A))
        {
            Footsteps.Stop();
            //Debug.Log(GameObject.FindWithTag("Player").transform.position);
        }
		
        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A))
        {
            Footsteps.Play();
        }
        position = GameObject.FindWithTag("Player").transform.position;
        //Debug.Log(position);
        //if (position.z > 14.0)
        //{
        //    Debug.Log("Grass now");
        //}
    }

    public void ChangeFootsteps(AudioClip steps)
    {
        Footsteps.Stop();
        Footsteps.clip = steps;
        Footsteps.Play();
    }
}
