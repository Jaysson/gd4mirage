﻿using System;
using UnityEngine;

public class SandArea : MonoBehaviour
{
    public enum Mode
    {
        Default,
        Optimized
    }

    struct Pos
    {
        public int x;
        public int y;
        public int z;
    }

    public float dimension = 0;
    public Material material;
    public Mode mode;

    private Vector3 colliderTransform;
    private Vector3 colliderScale;
    private Vector3 collidercorner;
    private Vector3 cubeScale;

    private int x;
    private int y;
    private int z;

    // Use this for initialization
    void Start()
    {
        colliderTransform = gameObject.transform.position;
        colliderScale = gameObject.transform.localScale;
        collidercorner = colliderTransform + new Vector3(-colliderScale.x, -colliderScale.y, -colliderScale.z) / 2;
        cubeScale = new Vector3(dimension, dimension, dimension);

        x = Mathf.FloorToInt(colliderScale.x / dimension);
        y = Mathf.FloorToInt(colliderScale.y / dimension);
        z = Mathf.FloorToInt(colliderScale.z / dimension);

        Debug.Log("Number of cubes: " + x * y * z);
        Debug.Log(x + " " + y + " " + z);

        for (int i = 0; i < x; ++i)
        {
            for (int j = 0; j < y; ++j)
            {
                for (int k = 0; k < z; ++k)
                {
                    switch (mode)
                    {
                        case Mode.Default:
                            GameObject box = GameObject.CreatePrimitive(PrimitiveType.Cube);
                            box.name = "SandBlock[" + i + "][" + j + "][" + k + "]";
                            box.tag = "blocks";
                            box.GetComponent<MeshRenderer>().material = material;
                            box.transform.position = collidercorner + new Vector3(cubeScale.x * i, cubeScale.y * j, cubeScale.z * k) + cubeScale / 2;
                            box.transform.localScale = cubeScale;
                            break;

                        case Mode.Optimized:
                            GameObject test = new GameObject();
                            test.transform.position = collidercorner + new Vector3(cubeScale.x * i, cubeScale.y * j, cubeScale.z * k) + cubeScale / 2;
                            test.transform.localScale = cubeScale;
                            test.name = "Collider[" + i + "][" + j + "][" + k + "]";
                            test.tag = (j < 3) ? "draw" : "none";
                            test.AddComponent<BoxCollider>();
                            test.GetComponent<BoxCollider>().isTrigger = true;
                            break;
                    }
                }
            }
        }

        if (mode == Mode.Optimized)
        {
            //DrawAllBlocks();
            DrawSomeBlocks();
            //DrawLine(0,0);
        }
    }

    void DrawAllBlocks()
    {
        for (int i = 0; i < x; ++i)
        {
            for (int j = 0; j < y; ++j)
            {
                DrawLine(i, j);
            }
        }
    }

    void DrawSomeBlocks()
    {
        for (int i = 0; i < x; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                DrawLine(i, j);
            }
        }
    }

    public void DrawLine(int j, int k)
    {
        Pos start = new Pos();
        Pos end = new Pos();
        int count = 0;

        bool hasStartPoint = false;
        

        //GameObject.Find("Collider[" + 2 + "][" + 2 + "][" + 2 + "]").tag = "none";
        //GameObject.Find("Collider[" + 2 + "][" + 2 + "][" + 3 + "]").tag = "none";
        //GameObject.Find("Collider[" + 2 + "][" + 2 + "][" + 4 + "]").tag = "none";
        //GameObject.Find("Collider[" + 2 + "][" + 2 + "][" + 7 + "]").tag = "none";

        for (int i = 0; i < z; ++i)
        {
            if (!hasStartPoint)
            {
                if (GameObject.Find("Collider[" + j + "][" + k + "][" + i + "]").tag == "draw")
                {
                    start.x = j;
                    start.y = k;
                    start.z = i;

                    hasStartPoint = true;
                }
            }
            else
            {
                if (i == z - 1)
                {
                    end.x = j;
                    end.y = k;
                    end.z = i;

                    draw(start, end);
                    ++count;
                    hasStartPoint = false;
                }
                else if (GameObject.Find("Collider[" + j + "][" + k + "][" + i + "]").tag != "draw")
                {
                    end.x = j;
                    end.y = k;
                    end.z = i - 1;

                    draw(start, end);
                    ++count;
                    hasStartPoint = false;
                }
            }
        }
    }

    public void RemoveLine(int x, int y)
    {
        
        foreach (Transform child in GameObject.Find("SandBlocks").transform)
        {
            if (child.name.StartsWith("SandBlock[" + x + "][" + y))
            {
                Destroy(child.gameObject);
            }
        }
        DrawLine(x, y);
    }

    void draw(Pos start, Pos end)
    {
        GameObject box = GameObject.CreatePrimitive(PrimitiveType.Cube);
        box.transform.parent = GameObject.Find("SandBlocks").transform;
        box.name = "SandBlock[" + start.x + "][" + start.y + "][" + (end.z - start.z + 1) + "]";
        box.GetComponent<MeshRenderer>().material = material;
        box.transform.position = collidercorner + new Vector3(cubeScale.x * start.x, cubeScale.y * start.y, cubeScale.z * (end.z + start.z) / 2) + cubeScale / 2;
        box.transform.localScale = new Vector3(dimension, dimension, (end.z - start.z + 1) * dimension);
    }

    void MovePointer(ref Pos pointer)
    {
        if (pointer.x < x)
        {
            pointer.x++;
        }
        else
        {
            if (pointer.y < y)
            {
                pointer.y++;
                pointer.x = 0;
            }
            else
            {
                if (pointer.z < z)
                {
                    pointer.z++;
                    pointer.y = 0;
                }
                else
                {

                }
            }
        }
        //Debug.Log(pointer.x + "," + pointer.y + "," + pointer.z);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
