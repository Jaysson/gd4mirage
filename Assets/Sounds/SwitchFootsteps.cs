﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchFootsteps : MonoBehaviour
{

    public AudioClip ConcreteSteps;

    private FootstepsManager FM;

    // Use this for initialization
    void Start()
    {
        FM = FindObjectOfType<FootstepsManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Grass")
        {            
                FM.ChangeFootsteps(ConcreteSteps);      
        }
    }
}
