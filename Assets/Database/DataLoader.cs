﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoader : MonoBehaviour
{

    public string[] session;

    IEnumerator Start()
    {
        WWW sessionData = new WWW("http://localhost/Jungian/ItemData.php");
        yield return sessionData;

        string sessionDataString = sessionData.text;
        print(sessionDataString);

        session = sessionDataString.Split(';');
        print(GetDataValue(session[0], "Name:"));
    }

    string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }
}
