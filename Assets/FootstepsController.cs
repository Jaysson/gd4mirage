﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsController : MonoBehaviour
{

    public AudioClip newTrack;
    public AudioClip newTrack1;
    public AudioClip newTrack2;
    private Vector3 position;
    private FootstepsSwitch theFC;

    // Use this for initialization
    void Start()
    {
        theFC = FindObjectOfType<FootstepsSwitch>();
    }

    // Update is called once per frame
    void Update()
    {
        position = GameObject.FindWithTag("Player").transform.position;
        //Debug.Log(position);
        if(position.z > 14.0)
        {
            //Debug.Log("Grass now");
            theFC.ChangeSteps(newTrack);
        }
        else if(position.z > 7.8 || position.x > 8.2)
        {
            //Debug.Log("Concrete now");
            theFC.ChangeSteps(newTrack1);
        }
        else
        {
            //Debug.Log("Indoor now");
            theFC.ChangeSteps(newTrack2);
        }
    }
}
*/