﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInGameMenu : MonoBehaviour {

    public CheckPlayType checkPlayType;    //instance of checkplaytype class so i can use the isVRMode variable
    public GameObject VRInGameMenu; //In VR game menu object attached in Unity.
    public GameObject KMInGameMenu; //In KM game menu object attached in Unity.
    public ShowEditableUI ShowEditableUI; //instance of the showeditableUI class, attached in unity.
    public GameObject SoundsScreen;
    public GameObject ControlsScreen;
    public GameObject AddModelsScreen;
    public Tutorials tutorials;
    private bool isOtherUIElementsEnabled; //bool for making sure that the escape keypress cannot bring up the main menu while in any other menu, therby trapping the player.

    // Use this for initialization
    void Start () {
        KMInGameMenu.SetActive(false);
        VRInGameMenu.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) && isOtherUIElementsEnabled == false )  //if escape is pressed the menu will be shown.
        {
            showInGameMenu();
            tutorials.hasAccMenu = 1;
        }
    }

    public void showInGameMenu()
    {
        if (ShowEditableUI.isShowEditableUIActive == false)
        {
            if (checkPlayType.isVRMode == true)
            {
                VRInGameMenu.SetActive(true); //Sets the menu object visibility to true, if the editableUI is not toggled already.
            }
            else if (checkPlayType.isVRMode == false)
            {
                KMInGameMenu.SetActive(true); //Sets the menu object visibility to true, if the editableUI is not toggled already.
            }
        }
        
    }

    public void hideInGameMenu()
    {
        if (checkPlayType.isVRMode == true)
        {
            VRInGameMenu.SetActive(false); //Sets the menu object visibility to false.
        }
        else if (checkPlayType.isVRMode == false)
        {
            KMInGameMenu.SetActive(false); //Sets the menu object visibility to false.
        }
    }

    public void ShowControlsScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        isOtherUIElementsEnabled = true;
        ControlsScreen.SetActive(true);
        KMInGameMenu.SetActive(false);        
    }

    public void ShowSoundsScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        isOtherUIElementsEnabled = true;
        SoundsScreen.SetActive(true);
        KMInGameMenu.SetActive(false);
    }

    public void ReturnFromSoundsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        isOtherUIElementsEnabled = false;
        SoundsScreen.SetActive(false);
        KMInGameMenu.SetActive(true);
    }

    public void ReturnFromControlsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        isOtherUIElementsEnabled = false;
        ControlsScreen.SetActive(false);
        KMInGameMenu.SetActive(true);
    }
    public void ShowAddModelssScreen() // this method enables the controls screen object and then disables the ingamemenu object
    {
        isOtherUIElementsEnabled = true;
        AddModelsScreen.SetActive(true);
        KMInGameMenu.SetActive(false);
    }

    public void ReturnFromAddModelsScreen() // this method disenables the controls screen object and then re-enables the ingamemenu object
    {
        isOtherUIElementsEnabled = false;
        AddModelsScreen.SetActive(false);
        KMInGameMenu.SetActive(true);
    }



    public void InGameExitGame()
    {
        //Exits the Game, Only when built as .exe , NOT IN THE EDITOR.
        Application.Quit();
    }
}
