﻿using System;
using UnityEngine;

public class SpawnBlocks : MonoBehaviour
{
    public SandArea sandArea;
    public Tutorials tutorials;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "none")
        {
            col.gameObject.tag = "draw";
            string x = "";
            string y = "";

            bool foundX = false;
            bool foundY = false;

            for (int i = 9; i < col.gameObject.name.Length; ++i)
            {
                if (Char.IsNumber(col.gameObject.name[i]) && !foundX)
                {
                    x += col.gameObject.name[i];
                }
                else if (Char.IsNumber(col.gameObject.name[i]) && foundX && !foundY)
                {
                    y += col.gameObject.name[i];
                }

                if (col.gameObject.name[i] == ']' && !foundX)
                {
                    foundX = true;
                }
                else if (col.gameObject.name[i] == ']' && foundX)
                {
                    foundY = true;
                }
            }
            tutorials.hasAddedSand = 1;
            sandArea.RemoveLine(Convert.ToInt32(x), Convert.ToInt32(y));
        }
    }
}