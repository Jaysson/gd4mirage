﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifiableManager : MonoBehaviour
{

    public List<GameObject> models = new List<GameObject>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void deleteList()
    {
        models.Clear();
    }

    public void switchSlotOne() //switches the model on the Slot One Array
    {
        if(models[0].GetComponent<ModifiableModel>() == true)
        { 
            Debug.Log("BING");
            if (models[0].GetComponent<ModifiableModel>().currentActiveSlotOne < models[0].GetComponent<customaztion>().slotOne.Length - 1) // makes sure the active slot does not reach an empty value thereby showing no model at all when changed
            {
                models[0].GetComponent<ModifiableModel>().currentActiveSlotOne++; //increments the current SlotOne number
                for (int i = 0; i < models[0].GetComponent<customaztion>().slotOne.Length; i++)
                {
                    if (i == models[0].GetComponent<ModifiableModel>().currentActiveSlotOne)
                    {
                        models[0].GetComponent<customaztion>().slotOne[i].SetActive(true); //enables the SlotOne model logged in the array value that i currently is in unity
                    }
                    else if (i != models[0].GetComponent<ModifiableModel>().currentActiveSlotOne)
                    {
                        models[0].GetComponent<customaztion>().slotOne[i].SetActive(false);//disables the SlotOne model logged in the array value that i currently is not in unity
                    }
                    //Debug.Log(currentActiveSlotOne);
                }
            }
        }
        else
        {
            Debug.Log("Not An Editable Model");
        }
    }

    public void switchSlotOneBack() //switches the model on the Slot One Array
    {
        //Debug.Log(slotOne.Length);
        if (models[0].GetComponent<ModifiableModel>() == true)
        {
            if (models[0].GetComponent<ModifiableModel>().currentActiveSlotOne < models[0].GetComponent<customaztion>().slotOne.Length + 1 && models[0].GetComponent<ModifiableModel>().currentActiveSlotOne != 0) // makes sure the active slot does not reach an empty value thereby showing no model at all when changed
            {
                //Debug.Log(modifableModel.currentActiveSlotOne);
                models[0].GetComponent<ModifiableModel>().currentActiveSlotOne--; //increments the current SlotOne number
                for (int i = 0; i < models[0].GetComponent<customaztion>().slotOne.Length; i++)
                {
                    if (i == models[0].GetComponent<ModifiableModel>().currentActiveSlotOne)
                    {
                        models[0].GetComponent<customaztion>().slotOne[i].SetActive(true); //enables the SlotOne model logged in the array value that i currently is in unity
                    }
                    else if (i != models[0].GetComponent<ModifiableModel>().currentActiveSlotOne)
                    {
                        models[0].GetComponent<customaztion>().slotOne[i].SetActive(false);//disables the SlotOne model logged in the array value that i currently is not in unity
                    }
                    //Debug.Log(currentActiveSlotOne);
                }
            }
        }
        else
        {
            Debug.Log("Not An Editable Model");
        }
    }

    public void switchSlotTwo() //switches the model on the Second Slot Array
    {
        if (models[0].GetComponent<ModifiableModel>() == true)
        {
            if (models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo < models[0].GetComponent<customaztion>().slotTwo.Length - 1) // makes sure the active slot does not reach an empty value thereby showing no model at all when changed
            {
                models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo++; //increments the current SlotTwo number
                for (int i = 0; i < models[0].GetComponent<customaztion>().slotTwo.Length; i++)
                {
                    if (i == models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo)
                    {
                        models[0].GetComponent<customaztion>().slotTwo[i].SetActive(true); //enables the slotTwo model logged in the array value that i currently is
                    }
                    else if (i != models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo)
                    {
                        models[0].GetComponent<customaztion>().slotTwo[i].SetActive(false); //disables the SlotTwo model logged in the array value that i currently is not in unity
                    }
                }
            }
        }
        else
        {
            Debug.Log("Not An Editable Model");
        }
    }

    public void switchSlotTwoBack() //switches the model on the Slot One Array
    {
        //Debug.Log(slotOne.Length);
        if (models[0].GetComponent<ModifiableModel>() == true)
        {
            if (models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo < models[0].GetComponent<customaztion>().slotTwo.Length + 1 && models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo != 0) // makes sure the active slot does not reach an empty value thereby showing no model at all when changed
            {
                //Debug.Log(modifableModel.currentActiveSlotTwo);
                models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo--; //increments the current SlotOne number
                for (int i = 0; i < models[0].GetComponent<customaztion>().slotTwo.Length; i++)
                {
                    if (i == models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo)
                    {
                        models[0].GetComponent<customaztion>().slotTwo[i].SetActive(true); //enables the SlotOne model logged in the array value that i currently is in unity
                    }
                    else if (i != models[0].GetComponent<ModifiableModel>().currentActiveSlotTwo)
                    {
                        models[0].GetComponent<customaztion>().slotTwo[i].SetActive(false);//disables the SlotOne model logged in the array value that i currently is not in unity
                    }
                    //Debug.Log(currentActiveSlotOne);
                }
            }
        }
        else
        {
            Debug.Log("Not An Editable Model");
        }
    }
}
