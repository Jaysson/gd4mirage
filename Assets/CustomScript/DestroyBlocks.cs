﻿using System;
using UnityEngine;

public class DestroyBlocks : MonoBehaviour
{
    public SandArea sandArea;
    public Tutorials tutorials;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "blocks")
        {
            Destroy(col.gameObject);
            tutorials.hasDeletedSand = 1;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "draw")
        {
            col.gameObject.tag = "none";
            string x = "";
            string y = "";

            bool foundX = false;
            bool foundY = false;

            for (int i = 9; i < col.gameObject.name.Length; ++i)
            {
                if (Char.IsNumber(col.gameObject.name[i]) && !foundX)
                {
                    x += col.gameObject.name[i];
                }
                else if (Char.IsNumber(col.gameObject.name[i]) && foundX && !foundY)
                {
                    y += col.gameObject.name[i];
                }

                if (col.gameObject.name[i] == ']' && !foundX)
                {
                    foundX = true;
                }
                else if (col.gameObject.name[i] == ']' && foundX)
                {
                    foundY = true;
                }
            }
            tutorials.hasDeletedSand = 1;
            sandArea.RemoveLine(Convert.ToInt32(x), Convert.ToInt32(y));
        }
    }
}