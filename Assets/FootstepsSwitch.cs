﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsSwitch : MonoBehaviour
{
    public AudioSource steps;

    // Use this for initialization
    void Start(){

    }

    // Update is called once per frame
    void Update(){

    }

    public void ChangeSteps(AudioClip music)
    {
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A))
        {
            steps.Stop();
            //Debug.Log(GameObject.FindWithTag("Player").transform.position);
        }

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A))
        {
            //steps.Stop();
            steps.clip = music;
            steps.Play();
        }
        
    }
}