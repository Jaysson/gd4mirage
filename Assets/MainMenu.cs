﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject mainscreen;
    public GameObject loginscreen;

    void Start()
    {
        loginscreen.SetActive(false); 
    }

    public void StartGame ()
    {
        //Loading the Main Room Scene, (If new scenes are added later on they also must be added in unity build settings as well as these lines of code.)
        SceneManager.LoadScene("MainRoom");
        
    }

    public void ExitGame ()
    {
        //Exits the Game, Only when built as .exe , NOT IN THE EDITOR.
        Application.Quit();
    }

    public void GoToLoginScreen()
    {
        mainscreen.SetActive(false);
        loginscreen.SetActive(true);
    }

    public void ExitFromLoginScreen()
    {
        loginscreen.SetActive(false);
        mainscreen.SetActive(true);
    }
}
