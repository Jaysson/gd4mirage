﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoginSystem : MonoBehaviour
{

    public GameObject username;
    public GameObject password;

    private string Username;
    private string Password;
    private string[] Lines;
    private string DecryptedPassword;

    public void Login()
    {
        bool UN = false;
        bool PW = false;

        if (Username != "")
        {
            if(System.IO.File.Exists(@"C:/College/UnityTestFolder/" + Username + ".txt"))
            {
                UN = true;
                Lines = System.IO.File.ReadAllLines(@"C:/College/UnityTestFolder/" + Username + ".txt");
            }
            else
            {
                Debug.LogWarning("Username Invalid");
            }
        }
        else
        {
            Debug.LogWarning("Username Field Blank");
        }
        if(Password != "")
        {
            if(System.IO.File.Exists(@"C:/College/UnityTestFolder/" + Username + ".txt"))
            {
            int i = 1;
                foreach (char c in Lines[2])
                {
                    i++;
                    char Decrypted = (char)(c / i);
                    DecryptedPassword += Decrypted.ToString();
                }
            }
            if(Password == DecryptedPassword)
            {
                PW = true;
            }
            else
            {
                Debug.LogWarning("Password field invalid");
            }
        }
        else
        {
            Debug.LogWarning("Password field empty");
        }

        if(UN == true && PW == true)
        {
            username.GetComponent<InputField>().text = "";
            password.GetComponent<InputField>().text = "";
            print("Login");
           // Application.LoadLevel("MainRoom");
            SceneManager.LoadScene("MainRoom");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Tab
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (username.GetComponent<InputField>().isFocused)
            {
                password.GetComponent<InputField>().Select();
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (Password != "")
            {
                Login();
            }
        }

        Username = username.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
    }
}
