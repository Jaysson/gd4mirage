﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseManager : MonoBehaviour
{

    public AudioMixerSnapshot paused;
    public AudioMixerSnapshot unpaused;
    public AudioMixerSnapshot outside;
    public AudioMixerSnapshot inside;
    private Vector3 position;
    private int count = 1;

    public Canvas canvas;

    void Start()
    {
        //canvas = GetComponent<Canvas>();
    }

    void Update()
    {
        position = GameObject.FindWithTag("Player").transform.position;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //canvas.enabled = !canvas.enabled;
            //canvas.gameObject.SetActive(true);
            //Pause();            
            //canvas = canvas.GetComponent<Canvas>();
            //Debug.Log(canvas);


            if (canvas.enabled == true)
            {
                paused.TransitionTo(0.1f);
                // Debug.Log(1);
                //Debug.Log(canvas.enabled);
            }
            else if (canvas.enabled == false)
            {
                unpaused.TransitionTo(0.1f);
                //Debug.Log(2);
                //Debug.Log(canvas.enabled);
            }
        }

        if (position.x > 8.5)
        {
            outside.TransitionTo(0.1f);
            //Debug.Log("Outside");
            count++;
        }
    }

    public void unpause()
    {

        unpaused.TransitionTo(0.1f);
    }

    //public void Pause()
    //{
    //    Time.timeScale = Time.timeScale == 0 ? 1 : 0;
    //    Lowpass();

    //}

    //void Lowpass()
    //{
    //    if(Time.timeScale == 0)
    //    {
    //        paused.TransitionTo(.01f);
    //    }
    //    else
    //    {
    //        unpaused.TransitionTo(.01f);
    //    }
    //}

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}