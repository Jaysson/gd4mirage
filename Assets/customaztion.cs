﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class customaztion : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    
    public GameObject[] slotOne; //Array of the First Slot of Changable Models
    public GameObject[] slotTwo; //Array of the Second Slot of Changable Models

    public ShowEditableUI editableModelUI; //A instance of the showCanvas script to access its methods which are called in the Update() function.
    public PickUp pickUp; //Instance of the pickup script to access its isHolding bool variable.
    public ModifiableModel modifableModel;
    //private int currentActiveSlotOne = 0; // Number value of the current SlotOne model in the slotOne array, 
    //private int currentActiveSlotTwo = 0;// Number value of the current SlotTwo model in the slotTwo array,
    private int toggleCounter = 0; //int used to implement a toggle press for the show canvas button.

    private bool showUI = false;

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey(KeyCode.E)) //If E is held down the UI Canvas will be set active and display.
        //{
        //    showCanvas(); // calls the showCanvas script to enable the UI canvas for editing a model
        //}
        //else
        //{
        //    hideCanvas(); // calls the hideCanvas script to disable the UI canvas for editing a model
        //}
        if (Input.GetKeyDown(KeyCode.E))
        {
            toggleUI();
        }
        if (showUI == true) //If E is held down the UI Canvas will be set active and display.
        {
            editableModelUI.showEditableUI(); // calls the showEditableUI script to enable the UI  for editing a model
        }
        else if (showUI == false)
        {
            editableModelUI.hideEditableUI(); // calls the hideEditableUI script to disable the UI  for editing a model
        }
    }

    private void toggleUI()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            showUI = true;
            toggleCounter++;
        }
    
        if ((toggleCounter % 2) == 0)
        {
            showUI = false;
        }
    }
    

    
}
