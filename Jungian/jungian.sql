-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2018 at 09:19 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jungian`
--

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `ID` int(11) NOT NULL,
  `Name` tinytext NOT NULL,
  `Comments` varchar(500) NOT NULL,
  `Date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`ID`, `Name`, `Comments`, `Date`) VALUES
(1, 'John Doe', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2018-10-27 11:32:46'),
(2, 'Jane Smith', 'Integer sollicitudin sollicitudin semper.', '2018-10-27 11:33:22'),
(3, 'Test name', 'Test comment', '2018-11-14 07:17:00'),
(4, 'Test name', 'Test comment', '2018-11-14 07:17:54'),
(5, 'Unity Test', 'Unity Test Comment', '2018-11-14 07:31:37'),
(6, 'Unity Test', 'Unity Test Comment', '2018-11-14 07:31:38'),
(7, 'Unity Test 1', 'Unity Test Comment 1', '2018-11-14 07:32:08'),
(8, 'Unity Test 1', 'Unity Test Comment 1', '2018-11-14 07:32:09'),
(9, 'Unity Test 1', 'Unity Test Comment 1', '2018-11-14 07:32:11'),
(10, 'Unity Test 1', 'Unity Test Comment 1', '2018-11-14 07:32:12'),
(11, 'Unity Test', 'Unity Test Comment', '2018-11-14 07:32:32'),
(12, 'Unity Test', 'Unity Test Comment', '2018-11-14 07:33:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `name`, `email`, `password`) VALUES
(1, 'Arthur Morgan', 'arthur.morgan@loyalty.com', '123abc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
