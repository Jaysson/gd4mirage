﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayType : MonoBehaviour {  //this script checks which player object is active (VR OR Keyboard and Mouse) and activates the corresponding UI Canvas for the player.

    public GameObject VRPlayer;
    public GameObject VRCanvas;
    public GameObject KMPlayer;
    public GameObject KMCanvas;

    public bool isVRMode = false;

    // Use this for initialization
    void Start () {
        VRCanvas.SetActive(false);
        KMCanvas.SetActive(false);

        if (VRPlayer.activeSelf == true)
        {
            VRCanvas.SetActive(true);
            isVRMode = true;
        }
        else if (KMPlayer.activeSelf == true)
        {
            KMCanvas.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
