﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShowTileCount : MonoBehaviour
{

    public GameObject SandTileCount; //SandTileCount UI object, Attached in unity
    
    
    // Use this for initialization
    void Start()
    {
        SandTileCount.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))  //if X is pressed down the UI will be shown.
        {
            showTileCountUI();
        }
    }

    public void showTileCountUI()
    {
        SandTileCount.SetActive(true); //Sets the UI object visibility to true, 
    }
}
