﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTiles : MonoBehaviour
{
    public Transform spawnPoint;
    public Rigidbody prefab;
    public int limit;
    public float destroyBelowY;

    private int count;
    public int accessiblecount;

    void Update()
    {
        if (count < limit)
        {
            if (Input.GetKey("z"))
            {
                Rigidbody rigidPrefab;
                prefab.tag = "tiles";
                rigidPrefab = Instantiate(prefab, spawnPoint.position, spawnPoint.rotation) as Rigidbody;
                ++count;
            }
            
            if (OVRInput.Get(OVRInput.Button.One))
            {
                Rigidbody rigidPrefab;
                prefab.tag = "tiles";
                rigidPrefab = Instantiate(prefab, spawnPoint.position, spawnPoint.rotation) as Rigidbody;
                ++count;
            }
            
        }

        //Debug.Log("Current Tile Count:" + count);
        accessiblecount = count;

        //Source: https://answers.unity.com/questions/50208/loop-through-all-objects-with-same-tag.html
        var objects = GameObject.FindGameObjectsWithTag("tiles");
        foreach (var obj in objects)
        {
            if (obj.transform.position.y < destroyBelowY)
            {
                Destroy(obj);
                --count;
                //Debug.Log("Destroyed a tile!");
            }
        }
    }
}