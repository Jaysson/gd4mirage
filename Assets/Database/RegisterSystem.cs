﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class RegisterSystem : MonoBehaviour
{

    public GameObject username;
    public GameObject email;
    public GameObject password;
    public GameObject confirmPassword;

    private string Username;
    private string Email;
    private string Password;
    private string ConfirmPassword;
    private string form;
    private bool EmailValid = false;
    private string[] Characters = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                                   "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
                                   "1","2","3","4","5","6","7","8","9","0","_","-"};
    string CreateAccountURL = "http://localhost/Jungian/createAccount.php";

    public void Register()
    {
        bool UN = false;
        bool EM = false;
        bool PW = false;
        bool CP = false;

        if (Username != "")
        {
            if (!System.IO.File.Exists(@"C:/College/UnityTestFolder/" + Username + ".txt"))
            {
                UN = true;
            }
            else
            {
                Debug.LogWarning("Username taken!");
            }
        }
        else
        {
            Debug.LogWarning("Username field blank!");
        }

        if (Email != "")
        {
            EmailValidation();
            if(EmailValid)
            {
                if(Email.Contains("@"))
                {
                    if(Email.Contains("."))
                    {
                        EM = true;
                    }
                    else
                    {
                        Debug.LogWarning("Email field invalid !");
                    }
                }
                else
                {
                    Debug.LogWarning("Email field invalid !");
                }
            }
            else
            {
                Debug.LogWarning("Email field invalid !");
            }
        }
        else
        {
            Debug.LogWarning("Email field blank!");
        }

        if(Password != "" && Password.Length > 5)
        {
            PW = true;
        }
        else
        {
            Debug.LogWarning("Password must be at least 6 characters long !");
        }

        if(ConfirmPassword != "" && ConfirmPassword == Password)
        {
            CP = true;
        }
        else
        {
            Debug.LogWarning("Passwords don't match!");
        }

        if(UN == true && EM == true && PW == true && CP == true)
        {
            bool Clear = true;
            int i = 1;
            foreach(char c in Password)
            {
                if(Clear)
                {
                    Password = "";
                    Clear = false;
                }
                i++;
                char Encrypted = (char)(c * i);
                Password += Encrypted.ToString();
            }
            form = (Username + Environment.NewLine + Email + Environment.NewLine + Password);
            System.IO.File.WriteAllText(@"C:/College/UnityTestFolder/" + Username + ".txt", form);

            StartCoroutine(LoginToDB(Username, Email, Password));

            username.GetComponent<InputField>().text = "";
            email.GetComponent<InputField>().text = "";
            password.GetComponent<InputField>().text = "";
            confirmPassword.GetComponent<InputField>().text = "";
            print("Registeration Comnplete");
        }
    }


    IEnumerator LoginToDB(string usename, string email, string password)
    {
        //Sends messages send to php script
        WWWForm Form = new WWWForm();
        //Form.AddField("Name", Username);
        Form.AddField("Email", Email);
        Form.AddField("Password", Password);

        WWW CreateAcountWWW = new WWW(CreateAccountURL, Form);
        yield return CreateAccountURL;

        if (CreateAccountURL == null)
        {
            Debug.LogError("Cannot connect to account creation");
        }
        else
        {
            string CreateAccountReturn = CreateAcountWWW.text;
            if (CreateAccountReturn == "Success")
            {
                Debug.Log("Success: Account Created");
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        //Tab
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (username.GetComponent<InputField>().isFocused)
            {
                email.GetComponent<InputField>().Select();
            }
            else if (email.GetComponent<InputField>().isFocused)
            {
                password.GetComponent<InputField>().Select();
            }
            else if (password.GetComponent<InputField>().isFocused)
            {
                confirmPassword.GetComponent<InputField>().Select();
            }
        }

        if(Input.GetKeyDown(KeyCode.Return))
        {
            if(Password != "" && Email != "" && ConfirmPassword != "")
            {
                Register();
            }
        }

        Username = username.GetComponent<InputField>().text;
        Email = email.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
        ConfirmPassword = confirmPassword.GetComponent<InputField>().text;
    }

    void EmailValidation()
    {
        bool SW = false;
        bool EW = false;
        for(int i = 0; i < Characters.Length; i++)
        {
            if(Email.StartsWith(Characters[i]))
            {
                SW = true;
            }
        }
        for (int i = 0; i < Characters.Length; i++)
        {
            if (Email.StartsWith(Characters[i]))
            {
                EW = true;
            }
        }

        if(SW == true && EW == true)
        {
            EmailValid = true;
        }
        else
        {
            EmailValid = false;
        }
    }
}
