﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstatiateNewModels : MonoBehaviour
{
    public GameObject vehicles;
    public GameObject cutlery;
    public GameObject balls;
    public GameObject animals;
    public Tutorials tutorials;
    GameObject cloneModels;
    bool isfirsttimespawn = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void spawnVehicles()
    {
        if (isfirsttimespawn == false)
        {
            deleteCloneModels();
        }
        tutorials.hasAddedModels = 1;
        cloneModels = Instantiate(vehicles, new Vector3(7.4f, 0.17f, -4.28f), Quaternion.Euler(0, 270.358f, 0));
        isfirsttimespawn = false;
    }
    public void spawnAnimals()
    {
        if (isfirsttimespawn == false)
        {
            deleteCloneModels();
        }
        tutorials.hasAddedModels = 1;
        cloneModels = Instantiate(animals, new Vector3(7.4f, 0.17f, -4.28f), Quaternion.Euler(0, 270.358f, 0));
        isfirsttimespawn = false;
    }
    public void spawnCutlery()
    {
        if (isfirsttimespawn == false)
        {
            deleteCloneModels();
        }
        tutorials.hasAddedModels = 1;
        cloneModels = Instantiate(cutlery, new Vector3(7.4f, 0.17f, -4.28f), Quaternion.Euler(0, 270.358f, 0));
        isfirsttimespawn = false;
    }
    public void spawnBalls()
    {
        if (isfirsttimespawn == false)
        {
            deleteCloneModels();
        }
        tutorials.hasAddedModels = 1;
        cloneModels = Instantiate(balls, new Vector3(7.4f, 0.17f, -4.28f), Quaternion.Euler(0, 270.358f, 0));
        isfirsttimespawn = false;
    }
    public void deleteCloneModels()
    {
        Destroy(cloneModels);
    }
}
