﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowEditableUI : MonoBehaviour {
    //This Script is used to show/hide the Editable Model UI  for the Editable Models, the show/hide methods and UI object are used here so that they can be attached to a single Customize Manager instead of Every Editable Model

    public CheckPlayType checkPlayType;    //instance of checkplaytype class so i can use the isVRMode variable
    public GameObject VREditableModelUI; // Editable Model UI 
    public GameObject KMEditableModelUI; // Editable Model UI 
    public bool isShowEditableUIActive = false;
    // Use this for initialization
    void Start()
    {
        KMEditableModelUI.SetActive(false);
        VREditableModelUI.SetActive(false);
    }
    
    public void showEditableUI()
    {
        if (checkPlayType.isVRMode == true)
        {
            VREditableModelUI.SetActive(true); //Sets the UI canvas to true.
            isShowEditableUIActive = true;
        }
        else if (checkPlayType.isVRMode == false)
        {
            KMEditableModelUI.SetActive(true); //Sets the UI canvas to true.
            isShowEditableUIActive = true;
        }
    }

    public void hideEditableUI()
    {
        if (checkPlayType.isVRMode == true)
        {
            VREditableModelUI.SetActive(false); //Sets the UI canvas to false.
            isShowEditableUIActive = false;
        }
        else if (checkPlayType.isVRMode == false)
        {
            KMEditableModelUI.SetActive(false); //Sets the UI canvas to false.
            isShowEditableUIActive = false;
        }
    }
}
